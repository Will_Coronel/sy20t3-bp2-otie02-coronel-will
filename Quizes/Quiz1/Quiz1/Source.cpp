#include <iostream>
#include <time.h>
#include <stdlib.h>	
#include <vector>

using namespace std;

int setBet(int& bet, int& mmLeft, int& round, int& moneyEarned) {
	cout << "It's round " << round << "." << endl;
	while (true) {
		cout << "How much mm would you bet?" << endl;
		cout << "You have " << mmLeft << "." << endl;
		cout << "You have " << moneyEarned << " Yen." << endl;
		cin >> bet;
		if (bet > mmLeft || bet == 0) {
			cout << "Oi oi you don't have that much, give me a proper bet" << endl << endl;
		}
		else {
			cout << endl <<  "Let's start! Hehehehe~" << endl << endl;
			break;
		}
	}
	system("pause");
	system("cls");
	return bet;
}	

int set(vector<string>& playDeck, vector<string>& aiDeck) {
	vector<string>eDeck = {"Emperor", "Civilian", "Civilian", "Civilian", "Civilian"};
	vector<string>sDeck = {"Slave", "Civilian", "Civilian", "Civilian", "Civilian"};
	int side = 0;
	
	if (side <= 5) {
		playDeck = eDeck;
		aiDeck = sDeck;
		cout << "Emperor Card!" << endl << "You play the Emperor side!" << endl;
	}
	else if (side >= 6) {
		playDeck = sDeck;
		aiDeck = eDeck;
		cout << "Emperor Card!" << endl << "You play the Slave side!" << endl;
	}
	
	return side;
	system("pause");
	system("cls");
}

void aiChoose(int &a,const vector<string>& aiDeck){
	a = rand() % aiDeck.size();
	cout << "Tonegawa has chosen!" << endl << endl;	
}

void choose(int &p, const vector<string>& playDeck) {
	cout << "Your cards are: " << endl;
	for (int i = 0; i < playDeck.size(); i++) {
		cout << playDeck[i] << endl;
	}

	cout << endl << "Choose your card!" << endl;
	cout << "Type the number of the card you will play~" << endl;
	for (int x = 0; x < playDeck.size(); x++) {
		cout << "[" << (x + 1) << "] " << playDeck[x] << endl;
	}
	cin >> p;
	p -= 1;
	system("pause");
	system("cls");
}

void turnOrder(int side, const vector<string>& playDeck, const vector<string>& aiDeck, int &a, int &p) {  
	if (side <= 5) { //Didn't understand what a turn order is so I made it Emperor always first
		choose(p, playDeck);
		aiChoose(a, aiDeck);
	}
	else if (side >= 6) {
		aiChoose(a, aiDeck);
		choose(p, playDeck);
	}
}

void check(vector<string>& playDeck, vector<string>& aiDeck, int& a, int& p, int& mmLeft, int& round, int& moneyEarned, int& bet) {
	int winnings;
	while (true){ 
		cout << "The Tonegawa chose " << aiDeck[a]  << "!" << endl;
		cout << "You chose " << playDeck[p] << "!" << endl;
		
		if (playDeck[p] == "Emperor" && aiDeck[a] == "Slave") {
			cout << "You Lost! Say goodbye to your " << bet << "mm Proceed to next Round~" << endl;
			mmLeft -= bet;
			system("pause");
			system("cls");
			break;
		}
		else if (playDeck[p] == "Slave" && aiDeck[a] == "Civilian") {
			cout << "You Lost! Say goodbye to your " << bet << "mm Proceed to next Round~" << endl;
			mmLeft -= bet;
			system("pause");
			system("cls");
			break;
		}
		else if (playDeck[p] == "Emperor" && aiDeck[a] == "Civilian") {
			cout << "You Win! Hehehehe" << endl;
			winnings = 100000 * bet;
			moneyEarned += winnings;
			cout << "Here's your " << winnings << " Yen" << endl;
			system("pause");
			system("cls");
			break;
		}
		else if (playDeck[p] == "Civilian" && aiDeck[a] == "Slave") {
			cout << "You Win! Hehehehe" << endl;
			winnings = 100000 * bet;
			moneyEarned += winnings;
			cout << "Here's your " << winnings << " Yen" << endl;
			system("pause");
			system("cls");
			break;
		}
		else if (playDeck[p] == "Slave" && aiDeck[a] == "Emperor") {
			cout << "You Win! Hehehehe" << endl;
			winnings = 500000 * bet;
			moneyEarned += winnings;
			cout << "Here's your " << winnings << " Yen" << endl;
			system("pause");
			system("cls");
			break;
		}
		else if (playDeck[p] == aiDeck[a]) {
			cout << "It's a draw! Proceed to next card~" << endl;
			aiDeck.erase(aiDeck.begin() + a);
			playDeck.erase(playDeck.begin() + p);
			system("pause");
			system("cls");
			break;
		}
	}

}

void playRound(int& round, int& moneyEarned, int& mmLeft, int &bet, vector<string>& playDeck, vector<string>& aiDeck, int& side, int& a, int& p) {
	setBet(bet, mmLeft, round, moneyEarned);
	set(playDeck, aiDeck);
	//choose(p, playDeck);
	//aiChoose(a, aiDeck);
	turnOrder(side, playDeck, aiDeck, a, p);
	check(playDeck, aiDeck, a, p, mmLeft, round, moneyEarned, bet);
}

int main() {

	srand(time(0));

	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	vector<string>playDeck;
	vector<string>aiDeck;
	int bet;
	int side = 0;
	int a = 0;
	int p = 0;

	while (round != 12 && mmLeft != 0 && moneyEarned != 20000000) {
		
		playRound(round, moneyEarned, mmLeft, bet, playDeck, aiDeck, side, a, p);

		round++;
	}

	if (mmLeft != 0 && moneyEarned == 20000000) {
		cout << "You have " << moneyEarned << " Yen and " << mmLeft << "mm to spare." << endl;
		cout << "How benevolent of us to have given you this chance Kaijin. Hehehehe" << endl;
	}
	else if (mmLeft != 0 && moneyEarned != 20000000) {
		cout << "You have " << moneyEarned << " Yen and " << mmLeft << "mm to spare." << endl;
		cout << "We gave you a chance to get away with your dept paid but all you saved was your ear" << endl;
	}
	else if (mmLeft == 0) {
		cout << "You lost! Hahahahaha! how do I sound? Ohh wait, you can't really hear me well do you?" << endl;
		if (moneyEarned == 0) {
			cout << "Too greedy... Going home with less than what you came with." << endl;
		}
		else {
			cout << "At least you have " << moneyEarned << " Yen. Hehehehehe" << endl;
		}
	}

}