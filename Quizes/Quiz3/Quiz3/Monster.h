#pragma once
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

class Player;

class Monster {
public:
	//Monster();
	//Monster(string name, string clas, int hp, int pow, int vit, int agi, int dex, int bonus, int per);
	Monster();
	Monster(string name, string clas, int hp, int pow, int vit, int agi, int dex, int bon, int per);
	int damage; //Decided to place them here cause they ain't really a property? __xX!~C O N F U S I O N~!Xx__
	int hit;
	int weak;

	//Behaviors/Moves
	void weakness(Player* target);
	void attack(Player* target);
	void takeDamage(Player* target);
	void printStat(Monster* target);
	

	//Getter
	string getName();
	string getClas();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	int getBonus();
	int getPercent();

	//Setter
	void setName(string name);
	void setClas(string clas);
	void setHp(int value);
	void setPow(int value);
	void setVit(int value);
	void setAgi(int value);
	void setDex(int value);
	void setBonus(int value);
	void setPercent(int value);

private:
	string mName;
	string mClas;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mBonus;
	int mPercent;

};

