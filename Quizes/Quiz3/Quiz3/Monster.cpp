#include "Monster.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"

using namespace std;

Monster* monster;

Monster::Monster()
{
	mName = "-";
	mClas = "-";
	mHp = 0;
	mPow = 0;
	mVit = 0;
	mAgi = 0;
	mDex = 0;
	mBonus = 0;
	mPercent = 0;
}

Monster::Monster(string name, string clas, int hp, int pow, int vit, int agi, int dex, int bon, int per)
{
	mName = name;
	mClas = clas;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mBonus = bon;
	mPercent = per;
}

void Monster::weakness(Player* target) 	// set weakness
{
	//Weak
	if ((mClas == "Warrior") && (target->getClas() == "Mage")) weak = 1;
	if ((mClas == "Assassin") && (target->getClas() == "Warrior")) weak = 1;
	if ((mClas == "Mage") && (target->getClas() == "Assassin")) weak = 1;

	//Strong
	if ((mClas == "Warrior") && (target->getClas() == "Assassin")) weak = 0;
	if ((mClas == "Assassin") && (target->getClas() == "Mage")) weak = 0;
	if ((mClas == "Mage") && (target->getClas() == "Warrior")) weak = 0;

	if (weak == 1) mBonus = 1; //Set
	else if (weak == 0) mBonus = 0; //Reset?

}

void Monster::attack(Player* target) //Attack/Hit% & bonus
{
	int find;
	hit = (mDex / target->getAgi()) * 100; //hit% = (DEX of attacker / AGI of defender) * 100
	if (hit < 20) hit = 20; //Minimum hit%
	else if (hit > 80) hit = 80;

	find = rand() % (100 - 1) + 1;

	if (find < hit) {
		mPercent = 1;
	}
	else {
		mPercent = 0;
	}

}

void Monster::takeDamage(Player* target) //compute damage taken & if Hit
{
	if (target->getBonus() == 1) {
		damage = (target->getPow() - mVit) + ((target->getPow() / 2));
	}
	else {
		damage = (target->getPow() - mVit);
	}

	if (target->getPercent() == 1) {
		cout << "Player Missed" << endl; return; //Miss
	}
	else if (target->getPercent() == 0) { //Hit

		if (damage < 1) return;
		mHp -= damage; //damage = (POW of attacker - VIT of defender) * bonusDamage
		if (mHp < 0) mHp = 0;
		cout << target->getName() << " attacks and deals " << damage << " damage." << endl;
	}
}

void Monster::printStat(Monster* target)
{
	cout << "-----------------------------------" << endl << endl;
	cout << "   Name: " << target->mName << endl;
	cout << "   Class: " << target->mClas << endl;
	cout << "   HP: " << target->mHp << endl;
	cout << "   POW: " << target->mPow << endl;
	cout << "   VIT: " << target->mVit << endl;
	cout << "   AGI: " << target->mAgi << endl;
	cout << "   DEX: " << target->mDex << endl;
}

//Getters
string Monster::getName()
{
	return mName;
}

string Monster::getClas()
{
	return mClas;
}

int Monster::getHp()
{
	return mHp;
}

int Monster::getPow()
{
	return mPow;
}

int Monster::getVit()
{
	return mVit;
}

int Monster::getAgi()
{
	return mAgi;
}

int Monster::getDex()
{
	return mDex;
}

int Monster::getBonus()
{
	return mBonus;
}

int Monster::getPercent()
{
	return mPercent;
}

//Setters
void Monster::setName(string name)
{
	mName = name;
}

void Monster::setClas(string clas)
{
	mClas = clas;
}

void Monster::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Monster::setPow(int value)
{
	mPow = value;
}

void Monster::setVit(int value)
{
	mVit = value;
}

void Monster::setAgi(int value)
{
	mAgi = value;
}

void Monster::setDex(int value)
{
	mDex = value;
}

void Monster::setBonus(int value)
{
	mBonus = value;
}

void Monster::setPercent(int value)
{
	mPercent = value;
}


