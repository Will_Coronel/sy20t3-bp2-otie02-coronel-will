#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Monster.h"

using namespace std;

void spawn(int& round) {
	int who = rand() % (4 - 1) + 1;
	int inc = 3 * (round - 1);

	if (who == 1) Monster* monster = new Monster("Enemy Warrior", "Warrior", 28 + inc, 7 + inc, 1 + inc, 7 + inc, 1 + inc, 1, 0);
	if (who == 2) Monster* monster = new Monster("Enemy Assassin", "Assassin", 8 + inc, 9 + inc, 3 + inc, 5 + inc, 1 + inc, 1, 0);
	if (who == 3) Monster* monster = new Monster("Enemy Mage", "Mage", 22 + inc, 7 + inc, 4 + inc, 4 + inc, 5 + inc, 1, 0);
}

void playerPrint(Player* player) { //Fk it
	cout << "   Name: " << player->getName() << endl;
	cout << "   Class: " << player->getClas() << endl;
	cout << "   HP: " << player->getHp() << endl;
	cout << "   POW: " << player->getPow() << endl;
	cout << "   VIT: " << player->getVit() << endl;
	cout << "   AGI: " << player->getAgi() << endl;
	cout << "   DEX: " << player->getDex() << endl;
}

void monsterPrint(Monster* monster) {
	cout << "   Name: " << monster->getName() << endl;
	cout << "   Class: " << monster->getClas() << endl;
	cout << "   HP: " << monster->getHp() << endl;
	cout << "   POW: " << monster->getPow() << endl;
	cout << "   VIT: " << monster->getVit() << endl;
	cout << "   AGI: " << monster->getAgi() << endl;
	cout << "   DEX: " << monster->getDex() << endl;
}

int main() {
	srand(time(0));

	cout << "Tell me your name: ";
	string nem;
	cin >> nem;
	system("cls");

	int round = 1;
	int turn = 1;

	Player* player = new Player(); //Set
	Monster* monster = new Monster(); //Set

	delete player; //Not sure but it works
	delete monster;

	//delete player; delete monster;

	while (true) {
		cout << "Choose a class!" << endl << "          [1] Warrior" << endl << "          [2] Mage" << endl << "          [3] Assassin" << endl;
		int choose;
		cin >> choose;

		if (choose < 1 || choose > 3) { //Checker
			cout << "Input a proper option!" << endl; system("pause"); system("cls");
		}
		else if (choose == 1) { //Warrior
			//  player = new Player(Name, Class, HP, Power, Vit, Agi, Dex, Bonus, Percent)
	
			Player* player = new Player(nem, "Warrior", 30, 8, 1, 8, 2, 1, 0); system("cls");
			cout << "You chose Warrior!" << endl;
			break;
		}
		else if (choose == 2) { //Mage
			
			Player* player = new Player(nem, "Mage", 24, 8, 5, 5, 6, 1, 0); system("cls");
			cout << "You chose Mage!" << endl;
			break;
		}
		else if (choose == 3) { //Assassin
			
			Player* player = new Player(nem, "Assassin", 10, 10, 4, 6, 1, 1, 0); system("cls");
			cout << "You chose Assassin!" << endl;
			break;
		}
	}
	system("pause"); system("cls");

	spawn(round); // Initial spawn

	while (true) { //Game				//I have no idea why it always says uninitialized :D

		cout << "--------------Battle!--------------" << endl;
		cout << "-----------------------------------" << endl;
		playerPrint(player);
		cout << "-----------------------------------" << endl;
		monsterPrint(monster);
		cout << "-----------------------------------" << endl;
		cout << "-----------------------------------" << endl << endl;

		//player->printStat(player); WONT WORK...
		//monster->printStat(monster); 
		
		//Who attacks First
		if (player->getAgi() > monster->getAgi()) { //Player First
			player->weakness(monster);
			monster->weakness(player);

			player->attack(monster);
			monster->attack(player);

			cout << "              Turn " << turn << endl;
			monster->takeDamage(player);
			player->takeDamage(monster);
		}
		else { //Monster First
			monster->weakness(player);
			player->weakness(monster);

			monster->attack(player);
			player->attack(monster);

			cout << "              Turn " << turn << endl;
			monster->takeDamage(player);
			player->takeDamage(monster);
		}
		cout << endl << "-----------------------------------" << endl;
		cout << "-----------------------------------" << endl;

		if (player->getHp() < 1) break; //Player killed
		turn++;
		system("pause"); system("cls");

		if (monster->getHp() < 1) { // Win
			player->winner(monster);
			round++;
			delete monster;
			spawn(round);
			turn = 1;
			system("pause"); system("cls");
		}
	} 
	//MAKE TURN COUNTER
	cout << endl << "You died. You lasted " << round << " round." << endl;
	
}



