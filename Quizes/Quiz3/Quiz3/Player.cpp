#include "Player.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Monster.h"

using namespace std;

Player* player;

Player::Player()
{
	mName = "-";
	mClas = "-";
	mHp = 0;
	mPow = 0;
	mVit = 0;
	mAgi = 0;
	mDex = 0;
	mBonus = 0;
	mPercent = 0;
}

Player::Player(string name, string clas, int hp, int pow, int vit, int agi, int dex, int bon, int per)
{
	mName = name;
	mClas = clas;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mBonus = bon;
	mPercent = per;
}

void Player::weakness(Monster* target) 	// set weakness
{
	//Weak
	if ((mClas == "Warrior") && (target->getClas() == "Mage")) weak = 1;
	if ((mClas == "Assassin") && (target->getClas() == "Warrior")) weak = 1;
	if ((mClas == "Mage") && (target->getClas() == "Assassin")) weak = 1;

	//Strong
	if ((mClas == "Warrior") && (target->getClas() == "Assassin")) weak = 0;
	if ((mClas == "Assassin") && (target->getClas() == "Mage")) weak = 0;
	if ((mClas == "Mage") && (target->getClas() == "Warrior")) weak = 0;

	if (weak == 1) mBonus = 1; //Set
	else if (weak == 0) mBonus = 0; //Reset?

	if (max == 0) { //Set Heal ONCE
		max = (mHp * 0.3);
		heal = int(max);
	}
	
}

void Player::attack(Monster* target) //Attack/Hit% & bonus
{
	int find;
	hit = (mDex / target->getAgi()) * 100; //hit% = (DEX of attacker / AGI of defender) * 100
	if (hit < 20) hit = 20; //Minimum hit%
	else if (hit > 80) hit = 80; //Max hit%

	find = rand() % (100 - 1) + 1;

	if (find < hit) {
		mPercent = 1;
	}
	else {
		mPercent = 0;
	}

}

void Player::takeDamage(Monster* target) //compute damage taken & if Hit
{
	if (target->getBonus() == 1) { //Bonus Damage
		damage = (target->getPow() - mVit) + ((target->getPow() / 2));
	}
	else {
		damage = (target->getPow() - mVit);
	}

	if (target->getPercent() == 1) {
		cout << "Enemy Missed" << endl; return; //Miss
	}
	else if (target->getPercent() == 0) { //Hit
		if (damage < 1) return;
		mHp -= damage; //damage = (POW of attacker - VIT of defender) * bonusDamage
		if (mHp < 0) mHp = 0;

		cout << target->getName() << " attacks and deals " << damage << " damage." << endl;
	}
}

void Player::printStat(Player* target) //it keeps diplaying the enemy stats instead despite being "player->" ???
{
	cout << "--------------Battle!--------------" << endl;
	cout << "-----------------------------------" << endl;
	cout << "   Name: " << target->mName << endl;
	cout << "   Class: " << target->mClas << endl;
	cout << "   HP: " << target->mHp << endl;
	cout << "   POW: " << target->mPow << endl;
	cout << "   VIT: " << target->mVit << endl;
	cout << "   AGI: " << target->mAgi << endl;
	cout << "   DEX: " << target->mDex << endl;
}

void Player::winner(Monster* target)
{
	cout << target->getName() << " is defeated!" << endl;

	if (target->getClas() == "Warrior") {
		cout << endl << mName << " gains: " << endl << "POW: 3" << endl << "VIT: 3" << endl; 
		mPow += 3; mVit += 3;
	}
	if (target->getClas() == "Assassin") {
		cout << endl << mName << " gains: " << endl << "AGI: 3" << endl << "DEX: 3" << endl; 
		mAgi += 3; mDex += 3;
	}
	if (target->getClas() == "Mage") {
		cout << endl << mName << " gains: " << endl << "POW: 5" << endl; 
		mPow += 5;
	}

	mHp += heal; //Heal 30% HP
	if (mHp > max) mHp = max;
}


//Getters
string Player::getName()
{
	return mName;
}

string Player::getClas()
{
	return mClas;
}

int Player::getHp()
{
	return mHp;
}

int Player::getPow()
{
	return mPow;
}

int Player::getVit()
{
	return mVit;
}

int Player::getAgi()
{
	return mAgi;
}

int Player::getDex()
{
	return mDex;
}

int Player::getBonus()
{
	return mBonus;
}

int Player::getPercent()
{
	return mPercent;
}

//Setters
void Player::setName(string name)
{
	mName = name;
}

void Player::setClas(string clas)
{
	mClas = clas;
}

void Player::setHp(int value)
{
	mHp = value;
}

void Player::setPow(int value)
{
	mPow = value;
}

void Player::setVit(int value)
{
	mVit = value;
}

void Player::setAgi(int value)
{
	mAgi = value;
}

void Player::setDex(int value)
{
	mDex = value;
}

void Player::setBonus(int value)
{
	mBonus = value;
}

void Player::setPercent(int value)
{
	mPercent = value;
}


