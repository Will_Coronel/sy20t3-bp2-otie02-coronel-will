#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>	
#include "Node.h"

using namespace std;

Node* start(int num) { //note: so head?
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;
	Node* next = nullptr;

	for (int i = 0; i < num; i++) {
		string names;
		cout << "What's your name soldier? ";
		cin >> names;

		current = new Node;
		current->name = names;

		if (head != nullptr) {
			previous->next = current;
			previous = current;

			if (current->next == NULL) {
				current->next = head;
			}
		}
		else {
			head = current;
			previous = current;
		}
	}
	return head;
}

void print(int num, Node* head, int round) {
	Node* current1 = head;

	cout << "==========================================" << endl << "ROUND " << round << endl << "==========================================" << endl <<
		"Remaining Members: " << endl;

	for (int i = 0; i < num; i++) {
		cout << current1->name << endl;
		current1 = current1->next;
	}
	cout << endl;
}

Node* randstart(int num, Node* head) {
	int pick = rand() % num + 1; //who chooses

	for (int i = 0; i < pick; i++) {
		head = head->next;
	}
	return head;
}

int del(Node* head, int num) {
	int indexToRemove = rand() % num + 1; //chose which to remove 

	cout << "Result: " << endl << head->name << " has drawn " << indexToRemove << "." << endl;

	Node* toDelete = head; //set
	Node* previous1 = nullptr; //track

	for (int i = 0; i < indexToRemove; i++) {
		previous1 = toDelete;
		toDelete = toDelete->next; //cycle
	}
	previous1->next = toDelete->next; //reconnect cycled 

	cout << toDelete->name << " was eliminated." << endl;

	toDelete = nullptr; //It didn't work at first
	delete toDelete; //Had to swap these two and it did for some reason...

	return num;
}


void game(Node* head) {
	cout << "==========================================" << endl << "FINAL RESULT" << endl << "==========================================" << endl <<
		head->name << " will go to seek for reinforcements." << endl;
}

int main() {
	srand(time(NULL));
	int num = 0;
	int round = 1;

	cout << "We need to setup our defenses!" << endl << "Head out there one by one and the last to go will ask for reinforcements" << endl <<
		"How many are you here? ";
	cin >> num;
	cout << endl;

	//start(num); doesn't connect :|
	Node* head = start(num); //setup
	system("cls");

	do {

		head = randstart(num, head);

		print(num, head, round);

		del(head, num);

		num = (num - 1);
		round++;

		system("pause");
		system("cls");
	} while (num != 1);

	game(head);

}