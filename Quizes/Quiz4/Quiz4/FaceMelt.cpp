#include "FaceMelt.h"

FaceMelt::FaceMelt(Unit* actor)
	: Skill("Face Melt", actor)
{
}

void FaceMelt::activate(Unit* target)
{
	cout << getActor()->getName() << " used " << getName() << " against " << target->getName() << "!" << endl;
}
