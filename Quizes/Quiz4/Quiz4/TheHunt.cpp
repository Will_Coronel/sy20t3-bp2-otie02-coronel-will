#include "TheHunt.h"

TheHunt::TheHunt(Unit* actor)
	: Skill("The Hunt", actor)
{
}

void TheHunt::activate(Unit* target)
{
	cout << getActor()->getName() << " used " << getName() << " against " << target->getName() << "!" << endl;
}

