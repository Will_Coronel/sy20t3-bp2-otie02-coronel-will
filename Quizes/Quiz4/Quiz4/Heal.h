#pragma once
#include "Skill.h"
#include "Unit.h"

class Heal :
    public Skill
{
public:
    Heal(Unit* actor);

    void activate(Unit* target) override;

};

