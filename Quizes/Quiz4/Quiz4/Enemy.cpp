#include "Enemy.h"

Enemy::Enemy()
	: Unit()
{
}

Enemy::Enemy(string name, int hp, int mp, int pow, int vit, int agi, int dex)
	: Unit(name, hp, mp, pow, vit, agi, dex)
{
}

void Enemy::singleEnemy(Unit* target)
{
	double rawDamage = (getPow() * 2.2);
	int baseDamage = (int)rawDamage;
	int damage = (baseDamage - target->getVit());
	if (damage < 0) damage = 1;

	cout << getName() << " used 'Puncturing Arrow' against " << target->getName() << "! Dealt " << damage << " damage." << endl;
	target->setHp(target->getHp() - damage);
	setMp(getMp() - 5);
}

void Enemy::multiEnemy(Unit* target)
{
	double rawDamage = (getPow() * 2.2);
	int baseDamage = (int)rawDamage;
	int damage = (baseDamage - target->getVit());
	if (damage < 0) damage = 1;

	cout << target->getName() << " took " << damage << " damage." << endl;
	target->setHp(target->getHp() - damage);
	
}

void Enemy::healEnemy(Unit* target)
{
	double rawHeal = (target->getMHp() * 0.3);
	int healAmount = (int)rawHeal;

	cout << getName() << " used Divine Hand!" << target->getName() << " is healed for " << healAmount << "." << endl;
	target->setHp(target->getHp() + healAmount);
	setMp(getMp() - 3);
}

