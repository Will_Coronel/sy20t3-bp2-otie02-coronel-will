#pragma once
#include "Skill.h"
#include "Unit.h"

class FaceMelt :
    public Skill
{
public:
    FaceMelt(Unit* actor);

    void activate(Unit* target) override;
};

