#include "Heal.h"
#include "Skill.h"

Heal::Heal(Unit* actor)
	: Skill("Heal", actor)
{
}

void Heal::activate(Unit* target)
{

	if (getActor()->getName() == "Lili") {
		cout << getActor()->getName() << " used Jug of Life to heal " << target->getName() << "!" << endl;
	}
	else {
		cout << getActor()->getName() << " used Divine Palm to heal " << target->getName() << "!" << endl;
	}

}

