#include "Player.h"
#include "Unit.h"
#include "Skill.h"

class Player;

Player::Player()
	: Unit()
{
}

Player::Player(string name, int hp, int mp, int pow, int vit, int agi, int dex)
	: Unit(name, hp, mp, pow, vit, agi, dex)
{
}

void Player::singlePlayer(Unit* target)
{
	double rawDamage = (getPow() * 2.2);
	int baseDamage = (int)rawDamage;
	int damage = (baseDamage - target->getVit());
	if (damage < 0) damage = 1;

	cout << getName() << " used 'The Hunt' against " << target->getName() << "! Dealt " << damage << " damage." << endl;
	target->setHp(target->getHp() - damage);
	setMp(getMp() - 5);
}

void Player::multiPlayer(Unit* target)
{
	double rawDamage = (getPow() * 2.2);
	int baseDamage = (int)rawDamage;
	int damage = (baseDamage - target->getVit());
	if (damage < 0) damage = 1;

	cout << target->getName() << " took " << damage << " damage." << endl;
	target->setHp(target->getHp() - damage);
	
}

void Player::healPlayer(Unit* target)
{
	double rawHeal = (target->getMHp() * 0.3);
	int healAmount = (int)rawHeal;
	cout << getName() << " used Jug of Life!" << target->getName() << " is healed for " << healAmount << "." << endl;
	target->setHp(target->getHp() + healAmount);
	setMp(getMp() - 3);
}









