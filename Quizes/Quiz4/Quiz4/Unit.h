#pragma once
#include <string>
#include <vector>
#include "Skill.h"
using namespace std;

class Enemy;
class Player;

class Unit
{
public:
	Unit();
	Unit(string name, int hp, int mp, int pow, int vit, int agi, int dex);

	void attack(Unit* target);

	//Getters
	string getName();
	int getHp();
	int getMp();
	int getPow();
	int getAgi();
	int getVit();
	int getDex();
	int getMHp();
	int getMMp();

	//vector<Skill*>& getSkills();


	//Setters
	void setName(string name);
	void setHp(int hp);
	void setMp(int mp);
	void setPow(int pow);
	void setVit(int vit);
	void setAgi(int agi);
	void setDex(int dex);
	void setMHp(int mhp);
	void setMMp(int mmp);

private:
	string mName;
	int mHp;
	int mMp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mMaxHp;
	int mMaxMp;
	vector<Skill*> mSkills;
};