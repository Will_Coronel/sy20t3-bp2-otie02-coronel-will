#pragma once
#include "Unit.h"
#include <string>
#include <vector>

using namespace std;

class Player;

class Enemy :
    public Unit
{
public:
    Enemy();
    Enemy(string name, int hp, int mp, int pow, int vit, int agi, int dex);

    void singleEnemy(Unit* target);
    void multiEnemy(Unit* target);
    void healEnemy(Unit* target);
};

