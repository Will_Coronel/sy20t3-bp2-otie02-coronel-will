#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Unit;

class Skill
{
public:

	Skill(string name, Unit* actor);

	string getName();
	Unit* getActor();

	virtual void activate(Unit* target) = 0;

private:
	string mName;
	Unit* mActor;
};

