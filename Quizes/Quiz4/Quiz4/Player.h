#pragma once
#include "Unit.h"
#include <string>
#include <vector>

using namespace std;

class Enemy;

class Player :
    public Unit
{
public:
    Player();
    Player(string name, int hp, int mp, int pow, int vit, int agi, int dex);
    
    void singlePlayer(Unit* target);
    void multiPlayer(Unit* target);
    void healPlayer(Unit* target);
  
};

