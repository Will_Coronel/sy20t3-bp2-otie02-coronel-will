#pragma once
#include "Skill.h"
#include "Unit.h"

class TheHunt :
    public Skill
{
public:
    TheHunt(Unit* actor);

    void activate(Unit* target) override;
};

