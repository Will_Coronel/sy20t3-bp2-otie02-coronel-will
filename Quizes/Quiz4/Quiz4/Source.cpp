#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
#include <functional>
#include "Unit.h"
#include "Player.h"
#include "Enemy.h"

using namespace std;

int targetEnemy(int& reciever, vector<Enemy*>& enemies);
int targetPlayer(int& reciever, vector<Player*>& players);
void printMain(vector<Player*>& players, vector<Enemy*>& enemies);
void printSpec(vector<Player*>& players, Player* player);
void movePlayer(int& reciever, vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units);
void moveEnemy(int& reciever, vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units);
void checkDeath(vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units);

int main() { //Heroes of The Storm?
	srand(time(0));
	int attacker = rand() % (4 - 1) + 1;
	int reciever = 0;
	vector<Unit*> units;
	vector<Enemy*> enemies;
	vector<Player*> players;
	vector<Player*> healths;
	//Ran out of time to abstract. It kept making errors :D
	//Can't figure out how to do turn order so I had the players go first but buffed the enemies
	//It can be sorted but I can't figure out how to make the skills unique to the user :[

	Player* ill = new Player("Illidan", 24, 15, 12, 6, 10, 10);
	units.push_back(ill);
	players.push_back(ill);
	healths.push_back(ill);

	Player* etc = new Player("E.T.C", 30, 18, 8, 6, 6, 8); 
	units.push_back(etc);
	players.push_back(etc);
	healths.push_back(ill);

	Player* lili = new Player("LiLi", 20, 20, 6, 8, 10, 4); 
	units.push_back(lili);
	players.push_back(lili);
	healths.push_back(ill);

	Enemy* johan = new Enemy("Johanna", 41, 11, 9, 7, 7, 9); 
	units.push_back(johan);
	enemies.push_back(johan);

	Enemy* valla = new Enemy("Valla", 37, 16, 5, 12, 10, 10); 
	units.push_back(valla);
	enemies.push_back(valla);

	Enemy* khar = new Enemy("Kharazim", 31, 21, 9, 7, 11, 5); 
	units.push_back(khar);
	enemies.push_back(khar);

	//I feel like I did something wrong [Win condition bug?]
	while ((ill->getHp() != 0 && etc->getHp() != 0 && lili->getHp() != 0) || (johan->getHp() != 0 && valla->getHp() != 0 && khar->getHp() != 0)) {
		printMain(players, enemies);
		movePlayer(reciever, players, enemies, healths, units);
		checkDeath(players, enemies, healths, units);
		moveEnemy(reciever, players, enemies, healths, units);
		checkDeath(players, enemies, healths, units);
	}
	
	if (johan->getHp() == 0 && valla->getHp() == 0 && khar->getHp() == 0) {
		cout << "Congratulations! You won against the followers of Diablo" << endl;
	}
	else {
		cout << "You have lost to the followers of Diablo" << endl;
	}

}

int targetEnemy(int& reciever, vector<Enemy*>& enemies) { //Target Attack
	cout << endl << "Choose who to attack: " << endl;

	for (int i = 0; i < enemies.size(); i++) {
		Enemy* enemy = enemies[i];
		cout << "Press " << (i+1) << " to target " << enemy->getName() << endl;
	}
	while (true) {
		cin >> reciever;
		if (reciever < 1 || reciever > 3) {
			cout << "Please select a proper option." << endl;
		}
		else break;
	}
	reciever -= 1;
	return reciever;
}

int targetPlayer(int& reciever, vector<Player*>& players) { //Target Teammate "HEAL"
	cout << endl << "Choose who to attack: " << endl;

	for (int i = 0; i < players.size(); i++) {
		Player* player = players[i];
		cout << "Press " << (i + 1) << " to target " << player->getName() << endl;
	}
	while (true) {
		cin >> reciever;
		if (reciever < 1 || reciever > 3) {
			cout << "Please select a proper option." << endl;
		}
		else break;
	}
	reciever -= 1;
	return reciever;
}

void printMain(vector<Player*>& players, vector<Enemy*>& enemies) {
	cout << "===================================" << endl << "Team: Warcraft" << endl << "===================================" << endl;
	for (int i = 0; i < players.size(); i++) {
		Player* player = players[i];
		cout << player->getName() << " [HP: " << player->getHp() << "]" << endl;
	}
	cout << endl;
	cout << "===================================" << endl << "Team: Diablo" << endl << "===================================" << endl;
	for (int i = 0; i < enemies.size(); i++) {
		Enemy* enemy = enemies[i];
		cout << enemy->getName() << " [HP: " << enemy->getHp() << "]" << endl;
	}
	cout << endl;
	system("pause"); system("cls");
}

void printSpec(vector<Player*>& players, Player* player) {
	cout << "Name: " << player->getName() << endl;
	cout << "HP: " << player->getHp() << "/" << player->getMHp() << endl;
	cout << "MP: " << player->getMp() << "/" << player->getMMp() << endl;
	cout << "Pow: " << player->getPow() << endl;
	cout << "Vit: " << player->getVit() << endl;
	cout << "Dex: " << player->getDex() << endl;
	cout << "Agi: " << player->getAgi() << endl << endl;
}

//GET READY TO BE MESSY.			E.T.C USED _xxX!!-MOSHPIT-!!Xxx_
void movePlayer(int& reciever, vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units) {
	int input;
	for (int i = 0; i < players.size(); i++) {
		Player* player = players[i]; //Cycle which character plays?
		//ETC--------------------------------------------------------------------------------------------------------------
		if (player->getName() == "E.T.C") { 
			while (true) {
				printSpec(players, player);
				cout << "Choose and action..." << endl << "===================================" << endl;
				cout << "[1] Attack" << endl << "[2] Skill: Face Melt" << endl;
				cin >> input;
				if (input == 1) {
					targetEnemy(reciever, enemies);
					player->attack(enemies[reciever]); system("pause"); system("cls"); break;
				}
				else if (input == 2 && player->getMp() >= 5) {
					cout << player->getName() << " used 'Face Melt'." << endl;
					for (int i = 0; i < enemies.size(); i++) {
						Enemy* enemy = enemies[i];
						player->multiPlayer(enemies[i]);
					}
					player->setMp(player->getMp() - 4);
					system("pause"); system("cls"); break;
				}
				else if (input == 2) {
					cout << "Not enough Mana." << endl; system("pause"); system("cls");
				}
				else {
					cout << "Please select a proper option." << endl;
				}
			}
		}
		//Illidan---------------------------------------------------------------------------------------------------------
		else if (player->getName() == "Illidan") { 
			while (true) {
				printSpec(players, player);
				cout << "Choose and action..." << endl << "===================================" << endl;
				cout << "[1] Attack" << endl << "[2] Skill: The Hunt" << endl;
				cin >> input;
				if (input == 1) {
					targetEnemy(reciever, enemies);
					player->attack(enemies[reciever]); system("pause"); system("cls"); break;
				}
				else if (input == 2 && player->getMp() >= 4) {
					targetEnemy(reciever, enemies);
					player->singlePlayer(enemies[reciever]); system("pause"); system("cls"); break;
				}
				else if (input == 2) {
					cout << "Not enough Mana." << endl; system("pause"); system("cls");
				}
				else { 
					cout << "Please select a proper option." << endl;
				}
			}
		}
		//Lili-----------------------------------------------------------------------------------------------------------
		else { 
			while (true) {
				printSpec(players, player);
				cout << "Choose and action..." << endl << "===================================" << endl;
				cout << "[1] Attack" << endl << "[2] Skill: Jug of Life" << endl;
				cin >> input;
				if (input == 1) {
					targetEnemy(reciever, enemies);
					player->attack(enemies[reciever]); system("pause"); system("cls"); break;
				}
				else if (input == 2 && player->getMp() >= 3) {
					targetPlayer(reciever, players);
					player->healPlayer(players[reciever]); system("pause"); system("cls"); break;
				}
				else if (input == 2) {
					cout << "Not enough Mana." << endl; system("pause"); system("cls");
				}
				else {
					cout << "Please select a proper option." << endl;
				}
			}
		}
	}
}

void moveEnemy(int& reciever, vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units) {
	int j;
	int fitty;
	bool once = true;

	Player* temp; //swappy sort based on Health || healths[0] lowest
	for (int i = 1; i < healths.size(); i++) {
		for (int j = 0; j < (healths.size() - 1); j++) {
			if (healths[j]->getHp() > healths[j + 1]->getHp()) {
				temp = healths[j + 1];
				healths[j + 1] = healths[j];
				healths[j] = temp;
			}
		}
	} //I think it works

	while (true) {
		for (int i = 0; i < enemies.size(); i++) {
			j = rand() % (3 - 1) + 1;
			fitty = rand() % (50 - 1) + 1;

			Enemy * enemy = enemies[i];
			Player * player = players[j];

			
			//Johanna--------------------------------------------------------------------------------------------------------------
			if (enemy->getName() == "Johanna") {
				if (fitty < 50 && enemy->getMp() >= 4) {
					cout << enemy->getName() << " used 'Falling Sword'." << endl;
					for (int i = 0; i < players.size(); i++) {
						enemy->multiEnemy(players[i]);
					}
					enemy->setMp(enemy->getMp() - 4);
				}
				else {
					enemy->attack(players[j]);
				}
				system("pause"); system("cls"); 
			}
			//Valla--------------------------------------------------------------------------------------------------------------
			else if (enemy->getName() == "Valla") {
				if (fitty < 50 && enemy->getMp() >= 5) {
						enemy->singleEnemy(players[i]);
				}
				else {
					enemy->attack(players[j]);
				}
				system("pause"); system("cls"); 
			}
			//Kharazim--------------------------------------------------------------------------------------------------------------
			else {
				if (fitty < 50 && enemy->getMp() >= 3) {
					enemy->healEnemy(enemies[i]);
				}
				else {
					enemy->attack(players[j]);
				}
				system("pause"); system("cls"); 
			}
		}
		break;
	}
}

void checkDeath(vector<Player*>& players, vector<Enemy*>& enemies, vector<Player*>& healths, vector<Unit*> units)
{
	for (int i = 0; i < units.size(); i++) { //Delete Units

		if (units[i]->getHp() == 0) {
			cout << units[i]->getName() << " has fallen." << endl;
			delete units.at(i);
		}
		for (int i = 0; i < players.size(); i++) { //Delete Players
			if (players[i]->getHp() == 0) {
				delete players.at(i);
			}
		}
		for (int i = 0; i < healths.size(); i++) { //Delete Health Check
			if (healths[i]->getHp() == 0) {
				delete healths.at(i);
			}
		}
		for (int i = 0; i < enemies.size(); i++) { //Delete Enemies
			if (enemies[i]->getHp() == 0) {
				delete enemies.at(i);
			}
		}
	}
}



/*
Turn Order

I don't even know what I'm doing anymore
All these else "CHOOSE PROPERLY" things look bad
This took me way too long to make and it's not even great... 
*/
