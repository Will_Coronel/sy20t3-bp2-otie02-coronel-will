#include "Unit.h"
#include <iostream>
#include <string>

Unit::Unit()
{
	mName = "-";
	mHp = 0;
	mMp = 0;
	mPow = 0;
	mVit = 0;
	mAgi = 0;
	mDex = 0;
}

Unit::Unit(string name, int hp, int mp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mHp = hp;
	mMp = mp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mMaxHp = hp;
	mMaxMp = mp;
}

void Unit::attack(Unit* target)
{
	int hit;
	int yes;
	double rawDamage;
	int find;
	int crit;

	while (true) { //Reset?
		find = rand() % (100 - 1) + 1;
		crit = rand() % (100 - 1) + 1;
		break;
	}

	hit = (getDex() / target->getAgi()) * 100;
	if (hit < 20) hit = 20; 
	else if (hit > 80) hit = 80; 

	if (find < hit) {
		yes = 1;
	}
	else {
		yes = 0;
	}

	cout << getName() << " is atacking " << target->getName();
	
	if (yes == 1) {
		if (crit <= 20) {
			rawDamage = (getPow() + (getPow() * 0.2) * 1);
			int baseDamage = (int)rawDamage;
			int damage = (baseDamage - target->getVit());
			if (damage < 0) damage = 1;
			cout << "! Dealt a critical hit with " << damage << " damage." << endl;
			target->setHp(target->getHp() - damage);
		}
		else {
			rawDamage = (getPow() * 1);
			int baseDamage = (int)rawDamage;
			int damage = (baseDamage - target->getVit());
			if (damage < 0) damage = 1;
			cout << "! Dealt " << damage << " damage." << endl;
			target->setHp(target->getHp() - damage);
		}
		
	}
	else {
		cout << endl << "Attack Missed." << endl;
	}
	
}

//-------------------------------------------------------------------------------------------------
string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getMHp()
{
	return mMaxHp;
}

int Unit::getMMp()
{
	return mMaxMp;
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::setHp(int hp)
{
	mHp = hp;
	if (mHp < 0) mHp = 0;
	if (mHp > mMaxHp) mHp = mMaxHp;
}

void Unit::setMp(int mp)
{
	mMp = mp;
	if (mMp > mMaxMp) mMp = mMaxMp;
}

void Unit::setPow(int pow)
{
	mPow = pow;
}

void Unit::setVit(int vit)
{
	mVit = vit;
}

void Unit::setAgi(int agi)
{
	mAgi = agi;
}

void Unit::setDex(int dex)
{
	mDex = dex;
}

void Unit::setMHp(int mhp)
{
	mMaxHp = mhp;
}

void Unit::setMMp(int mmp)
{
	mMaxMp = mmp;
}

