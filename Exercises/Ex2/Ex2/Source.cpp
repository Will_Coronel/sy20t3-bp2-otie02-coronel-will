#include <iostream>
#include <time.h>
#include <stdlib.h>	
#include <vector>

using namespace std;

/*
void randInv(string item[], string inv[]) {
	
}
*/
/*
void showInv(string inv[]) {
	for (int i = 0; i < 10; i++) {
		cout << "Inventory: " << endl;
		cout << inv[i] << endl;
	}
}
*/

void set(vector<string>& item) {
	item.push_back("RedPotion");
	item.push_back("Elixir");
	item.push_back("EmptyBottle");
	item.push_back("BluePotion");
}

void fill(const vector<string>& item, vector<string>& inv) {
	for (int i = 0; i < 10; i++) {
		int x = rand() % 4;
		inv.push_back(item[x]);
	}
}

void show(const vector<string>& inv) {
	cout << "You're Inventory: " << endl;
	for (int i = 0; i < inv.size(); i++) {
		cout << inv[i] << endl;
	}
	cout << endl;
}

void count(const vector<string>& inv, vector<int>& c) {
	for (int x = 0; x < 4; x++) { //populate c
		c.push_back(0);
	}
	
	for (int i = 0; i < 10; i++) {
		if (inv[i] == "RedPotion") {
			c[0] += 1;
		}
		else if (inv[i] == "Elixir") {
			c[1] += 1;
		}
		else if (inv[i] == "EmptyBottle") {
			c[2] += 1;
		}
		else if (inv[i] == "BluePotion") {
			c[3] += 1;
		}
	}
	
	cout << "You have: " << endl;
	cout << "RedPotion = " << c[0] << endl;
	cout << "Elixir = " << c[1] << endl;
	cout << "EmptyBottle = " << c[2] << endl;
	cout << "BluePotion = " << c[3] << endl << endl;
	system("pause");
	system("cls");
}

void del(vector<string>& inv) {
	show(inv);
	cout << "Throw item from which slot? (ex: 1 'for first slot')" << endl;
	int y;
	cin >> y;
	inv.erase(inv.begin() + (y - 1));
	system("cls");
	show(inv);
	system("pause");
	system("cls");
	
}

int main() {
	srand(time(NULL));
	vector<string> item;
	vector<string> inv;
	vector<int> c;

	set(item);
	fill(item, inv);
	show(inv);
	count(inv, c);
	del(inv);
	
}