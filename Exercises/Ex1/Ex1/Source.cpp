#include <iostream>
#include <time.h>
#include <stdlib.h>		

using namespace std;


void bet(int& gold, int& pBet) {
	while (true) {
		cout << "Place your bet." << endl;
		cin >> pBet;

		if (pBet != 0 && pBet <= gold) {
			gold -= pBet;
			cout << "Your have " << gold << " gold left." << endl;
			break;
		}
		else if (pBet == 0) {
			cout << "You place an invalid BET! Bet more than 0." << endl;
			cout << "Your still have " << gold << " gold left." << endl << endl;
		}
		else if (pBet > gold) {
			cout << "You have place an invalid BET! Bet must be lower than your current gold." << endl;
			cout << "Your still have " << gold << " gold left." << endl << endl;
		}
		else {
			break;
		}	
	}
}

/*
int bet2(int& gold, int& pBet) {
	while (true) {
		cout << "Place your bet." << endl;
		cin >> pBet;

		if (pBet != 0 && pBet <= gold) {
			gold -= pBet;
			cout << "Your have " << gold << " gold left." << endl;
			break;
		}
		else if (pBet == 0) {
			cout << "You place an invalid BET! Bet more than 0." << endl;
			cout << "Your still have " << gold << " gold left." << endl << endl;
		}
		else if (pBet > gold) {
			cout << "You have place an invalid BET! Bet must be lower than your current gold." << endl;
			cout << "Your still have " << gold << " gold left." << endl << endl;
		}
		else {
			break;
		}
	}
	return pBet;
}
*/  
// Return thing

int roll(int& d1, int& d2) {
	d1 = rand() % 6 + 1;
	d2 = rand() % 6 + 1;
	int dP = d1 + d2;
	
	return dP;
}



void payout(int dP, int dA, int& gold, int& pBet, int d1, int d2) {
	if (dP > dA) {
		pBet = (pBet * 2);
		gold += pBet;
		cout << "You rolled higher and won " << pBet << "gold! You now have " << gold << " gold." << endl;
	}
	else if (gold == 0) {
		cout << "You rolled lower and lost." << endl;
		cout << "You no longer have any gold left..." << endl;
	}
	else if (d1 == 1 && d2 == 1) {
		pBet = (pBet * 3);
		gold += pBet;
		cout << "Snake eyes! You win 3x(" << pBet << ") your bet! You now have " << gold << "gold." << endl;
	}
	else if (dP == dA) {
		gold += pBet;
		cout << "It's a TIE! (Draw)" << "Your bet has been returned" << endl;
		cout << "You now have " << gold << " gold." << endl;
	}
	else if (dP < dA) {
		cout << "You rolled lower and lost." << endl;
		cout << "You still have " << gold << " gold." << endl;
	}
}

void playRound(int& gold, int& pBet, int d1, int d2) {
	while (gold > 0 && gold != 0) {
		int d1, d2;
		bet(gold, pBet);
		//bet2(gold, pBet);
		system("pause");
		system("cls");
		int dP = roll(d1, d2);
		cout << "You rolled: " << d1 << "-" << d2 << ". Total: " << dP << endl;
		system("pause");
		int dA = roll(d1, d2);
		cout << "AI rolled: " << d1 << "-" << d2 << ". Total: " << dA << endl;
		payout(dP, dA, gold, pBet, d1, d2);
		system("pause");
		system("cls");
	}
	cout << "You lost the game!" << endl;
}

int main(int d1, int d2) {
	srand(time(NULL));

	int gold = 1000;
	int pBet;

	playRound(gold, pBet, d1, d2);
	
}
