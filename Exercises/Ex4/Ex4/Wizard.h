#pragma once
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

class Spell;

class Wizard{
public:

	string mName;
	int mHp = 0;
	int mMp = 0;
	int mDamage = 0;
	
	void attack(Wizard* target);
	
};

