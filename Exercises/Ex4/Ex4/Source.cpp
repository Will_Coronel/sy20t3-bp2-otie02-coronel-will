#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "Spell.h"

using namespace std;

/*class wizard { //I think I've failed this misserably 
public:

	string mname;
	int mhp = 0;
	int mmp = 0;
	int mdamage = 0;

	void attack(wizard* target);
};

class spell {
public:

	string mname;
	int mdamage = 0;
	int mmpcost = 0;

	void activate(wizard* target);
};*/	

int main(){
	srand(time(0));

	Wizard* wizard1 = new Wizard();
	wizard1->mName = "Wizard 1";
	wizard1->mHp = 250;
	wizard1->mMp = 0;
	
	Wizard* wizard2 = new Wizard();
	wizard2->mName = "Wizard 2";
	wizard2->mHp = 250;
	wizard2->mMp = 0;

	Spell* spell1 = new Spell();
	spell1->mName = "Spell";
	spell1->mMpCost = 50;

	Spell* spell2 = new Spell();
	spell2->mName = "Spell";
	spell2->mMpCost = 50;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


	cout << "------------Begind the Battle!------------" << endl;

	do {
		wizard1->mDamage = rand() % (16 - 10) + 10;
		wizard2->mDamage = rand() % (16 - 10) + 10;
		spell1->mDamage = rand() % (61 - 40) + 40;
		spell2->mDamage = rand() % (61 - 40) + 40;
		//-----------------------------------------------------------------------------
		//-----------------------------------------------------------------------------

		cout << "------------------Battle------------------" << endl;
		cout << "------------------------------------------" << endl;
		cout << "Wizard 1 Status" << endl;
		cout << "HP: " << wizard1->mHp << endl;
		cout << "MP: " << wizard1->mMp << endl;
		cout << "------------------------------------------" << endl;
		cout << "Wizard 2 Status" << endl;
		cout << "HP: " << wizard2->mHp << endl;
		cout << "MP: " << wizard2->mMp << endl;
		cout << "------------------------------------------" << endl;
		system("pause");
		//-----------------------------------------------------------------------------
		//-----------------------------------------------------------------------------

		if (wizard1->mMp >= 50) {
			spell1->activate(wizard2, wizard1);
		}
		else  {
			wizard1->attack(wizard2);
		}

		if (wizard2->mHp <= 0) {
			cout << "Wizard 2's faints" << endl;
			break; 
		}

		if (wizard2->mMp >= 50) {
			spell2->activate(wizard1, wizard2);
		}
		else {
			wizard2->attack(wizard1);
		}

		system("pause");
		system("cls");


	} while ((wizard1->mHp >= 0 ) && (wizard2-> mHp >= 0));

	if (wizard1 > 0) {
		cout << wizard1->mName << " wins!" << endl << endl;
	}
	else {
		cout << wizard2->mName << " wins!" << endl << endl;
	}
}