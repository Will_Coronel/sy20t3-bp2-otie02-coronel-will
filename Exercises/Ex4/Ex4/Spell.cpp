#include "Spell.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"

using namespace std;

void Spell::activate(Wizard* target, Wizard* target2){
	
	target->mHp -= this->mDamage;
	cout << target2->mName << " uses a spell, dealing " << this->mDamage << " to " << target->mName << ". " << endl;
	target2->mMp -= this->mMpCost;
}
