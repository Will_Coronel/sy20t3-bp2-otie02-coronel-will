#include "Wizard.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Spell.h"

using namespace std;

void Wizard::attack(Wizard* target){

	int mpin = rand() % (20 - 10) + 10;
	this->mMp += mpin;
	target->mHp -= this->mDamage;
	cout << this->mName << " attacks, dealing " << this->mDamage << " to " << target->mName << ". " << this->mName << "'s mana increased by " << mpin << "." << endl;

}
