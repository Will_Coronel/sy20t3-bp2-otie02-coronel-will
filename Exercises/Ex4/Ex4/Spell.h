#pragma once
#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"

using namespace std;

class Spell{
public:

	string mName;
	int mDamage = 0;
	int mMpCost = 0;

	void activate(Wizard* target, Wizard* target2);

};

